_model: page
---
title: Post-install
---
body:

Congrats! If you get to this point, it means that your obfs4 bridge is running and is being distributed by BridgeDB to censored users. If you want to connect to your bridge manually, you will need to know the bridge's obfs4 certificate. See the file `/var/lib/tor/pt_state/obfs4_bridgeline.txt` and paste the entire bridge line into Tor Browser:

```
Bridge obfs4 <IP ADDRESS>:<PORT> <FINGERPRINT> cert=<CERTIFICATE> iat-mode=0
```

You'll need to replace `<IP ADDRESS>`, `<PORT>`, and `<FINGERPRINT>` with the actual values, which you can find in the tor log. Make sure to use `<FINGERPRINT>`, not `<HASHED FINGERPRINT>`; and that `<PORT>` is the one from the log line `Registered server transport 'obfs4'`, not the one from the line `Now checking whether ORPort ... is reachable`.

Finally, you can monitor your obfs4 bridge's usage on [Relay Search](https://metrics.torproject.org/rs.html#search).  Just enter your bridge's `<HASHED FINGERPRINT>` in the form and click "Search". After having set up the bridge, it takes approximately three hours for the bridge to show up in Relay Search.

---
html: two-columns-page.html
---
key: 5
---
subtitle: How to find your Bridge in Relay Search and connect manually
